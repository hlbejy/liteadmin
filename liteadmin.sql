/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50639
Source Host           : localhost:3306
Source Database       : liteadmin

Target Server Type    : MYSQL
Target Server Version : 50639
File Encoding         : 65001

Date: 2019-03-09 10:14:53
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for content_advs
-- ----------------------------
DROP TABLE IF EXISTS `content_advs`;
CREATE TABLE `content_advs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `cid` int(10) unsigned NOT NULL COMMENT '所属分类',
  `title` varchar(255) NOT NULL COMMENT '名称',
  `url` varchar(255) NOT NULL COMMENT '链接',
  `description` varchar(255) NOT NULL COMMENT '描述',
  `image` varchar(255) NOT NULL COMMENT '图片',
  `state` int(1) NOT NULL DEFAULT '1' COMMENT '1 启用',
  `create_time` int(11) NOT NULL,
  `is_deleted` int(1) NOT NULL DEFAULT '0' COMMENT '1 已删',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of content_advs
-- ----------------------------
INSERT INTO `content_advs` VALUES ('7', '6', 'asd as d', 'asd', 'asd', '', '1', '0', '0');
INSERT INTO `content_advs` VALUES ('8', '6', 'asdf', 'asdf', 'asdf', '', '1', '0', '1');
INSERT INTO `content_advs` VALUES ('9', '6', 'asd', 'asd', 'asd ', '', '1', '1547112582', '1');
INSERT INTO `content_advs` VALUES ('10', '0', 'asd as d', 'asd', 'asd', '', '1', '1547113047', '0');

-- ----------------------------
-- Table structure for content_advs_category
-- ----------------------------
DROP TABLE IF EXISTS `content_advs_category`;
CREATE TABLE `content_advs_category` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL COMMENT '分类名称',
  `description` varchar(255) NOT NULL COMMENT '描述',
  `state` int(255) NOT NULL DEFAULT '0' COMMENT '0 禁用',
  `is_deleted` int(1) NOT NULL COMMENT '1 已删',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of content_advs_category
-- ----------------------------
INSERT INTO `content_advs_category` VALUES ('6', 'asd as d', 'asdfasdfa', '1', '1');

-- ----------------------------
-- Table structure for content_article
-- ----------------------------
DROP TABLE IF EXISTS `content_article`;
CREATE TABLE `content_article` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cid` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `keyword` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `thumb` varchar(255) NOT NULL,
  `content` longtext NOT NULL,
  `md_content` longtext NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `is_deleted` tinyint(1) unsigned NOT NULL,
  `state` tinyint(1) unsigned NOT NULL COMMENT '显示隐藏',
  `click` int(11) NOT NULL COMMENT '点击量',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of content_article
-- ----------------------------
INSERT INTO `content_article` VALUES ('1', '5', '差不多可以了，今天我的新版博客正式上线。', '博客', '无', '/uploads/20181007/2018c8cbb31190505319c18fbeb6271a.jpg', '<p>前前后后折腾了有段日子了，一点一点也做出来了一个样子，过程也算是蛮曲折，今天开始就正式用上了</p>\n<p>后台项目 <a href=\"https://gitee.com/Mao02/liteadmin\" rel=\"nofollow noreferrer noopener\" target=\"_blank\">liteadmin</a></p>\n<p>这个博客是衍生在后台项目基础之上的一个内置模块。基本上使用这个后台系统所需要的演示都已经包含，当然根据现有的资源也许可以挖掘出其他更多的使用方式。</p>\n<p>旧的 <a href=\"http://blog.dazhetu.cn\" rel=\"nofollow noreferrer noopener\" target=\"_blank\">博客</a> 内容不准备迁移了，解析也不会取消，作为一个历史吧。</p>\n<p>那么，开始吧！</p>', '前前后后折腾了有段日子了，一点一点也做出来了一个样子，过程也算是蛮曲折，今天开始就正式用上了\r\n\r\n后台项目 [liteadmin](https://gitee.com/Mao02/liteadmin)\r\n\r\n这个博客是衍生在后台项目基础之上的一个内置模块。基本上使用这个后台系统所需要的演示都已经包含，当然根据现有的资源也许可以挖掘出其他更多的使用方式。\r\n\r\n旧的 [博客](http://blog.dazhetu.cn) 内容不准备迁移了，解析也不会取消，作为一个历史吧。\r\n\r\n那么，开始吧！', '1538829385', '1539355749', '0', '1', '167');
INSERT INTO `content_article` VALUES ('2', '2', '使用迅搜xunsearch给我的博客加上中文全文搜索（一）', '全文检索', 'wu', '/uploads/20181010/16834b8dfcbc336085f41ed0636a80fc.jpg', '<p><a href=\"http://www.xunsearch.com/\" rel=\"nofollow noreferrer noopener\" target=\"_blank\">迅搜</a> 是一个开源免费、高性能、多功能，简单易用的专业全文检索技术方案。</p>\n<p>旨在帮助一般开发者针对既有的海量数据，快速而方便地建立自己的全文搜索引擎。全文检索可以帮助您降低服务器搜索负荷、极大程度的提高搜索速度和用户体验</p>\n<p>我的博客作为一个中小型项目，想要实现搜索功能应该是比较靠谱的解决方案。</p>\n<p>因为迅搜的核心服务需要在linux下运行，所以前提是要有一个linux的服务器。</p>\n<p>话不多说</p>\n<h2>安装</h2>\n<pre><code>wget http://www.xunsearch.com/download/xunsearch-full-latest.tar.bz2\ntar -xjf xunsearch-full-latest.tar.bz2\n# 先ll命令查看解压出来的目录名字，然后进入\ncd xunsearch-full-1.4.12/\nsh setup.sh</code></pre>\n<p>接着确认安装路径 直接就OK了，安装就是这么简单。</p>\n<p>我选择的默认安装路径 <code>/usr/local/xunsearch</code> ，所以服务的各项命令都在 <code>/usr/local/xunsearch/bin</code> 下。</p>\n<h2>启动服务</h2>\n<p>进入 bin 目录 启动命令如下</p>\n<pre><code>xs-ctl.sh -b local start    // 监听在本地回环地址 127.0.0.1 上\nxs-ctl.sh -b inet start     // 监听在所有本地 IP 地址上\nxs-ctl.sh -b a.b.c.d start  // 监听在指定 IP 上\nxs-ctl.sh -b unix start     // 分别监听在 tmp/indexd.sock 和 tmp/searchd.sock</code></pre>\n<h2>使用 SDK 开发项目</h2>\n<p>我采用的composer安装SDK开发包</p>\n<pre><code>composer require --prefer-dist hightman/xunsearch \"*@beta\"</code></pre>\n<p>准备工作就到这里，准备开码</p>', '[迅搜](http://www.xunsearch.com/) 是一个开源免费、高性能、多功能，简单易用的专业全文检索技术方案。\r\n\r\n旨在帮助一般开发者针对既有的海量数据，快速而方便地建立自己的全文搜索引擎。全文检索可以帮助您降低服务器搜索负荷、极大程度的提高搜索速度和用户体验\r\n\r\n我的博客作为一个中小型项目，想要实现搜索功能应该是比较靠谱的解决方案。\r\n\r\n因为迅搜的核心服务需要在linux下运行，所以前提是要有一个linux的服务器。\r\n\r\n话不多说\r\n\r\n## 安装\r\n\r\n```\r\nwget http://www.xunsearch.com/download/xunsearch-full-latest.tar.bz2\r\ntar -xjf xunsearch-full-latest.tar.bz2\r\n# 先ll命令查看解压出来的目录名字，然后进入\r\ncd xunsearch-full-1.4.12/\r\nsh setup.sh\r\n```\r\n\r\n接着确认安装路径 直接就OK了，安装就是这么简单。\r\n\r\n我选择的默认安装路径 `/usr/local/xunsearch` ，所以服务的各项命令都在 `/usr/local/xunsearch/bin` 下。\r\n\r\n## 启动服务\r\n\r\n进入 bin 目录 启动命令如下\r\n```\r\nxs-ctl.sh -b local start    // 监听在本地回环地址 127.0.0.1 上\r\nxs-ctl.sh -b inet start     // 监听在所有本地 IP 地址上\r\nxs-ctl.sh -b a.b.c.d start  // 监听在指定 IP 上\r\nxs-ctl.sh -b unix start     // 分别监听在 tmp/indexd.sock 和 tmp/searchd.sock\r\n```\r\n\r\n## 使用 SDK 开发项目\r\n\r\n我采用的composer安装SDK开发包\r\n```\r\ncomposer require --prefer-dist hightman/xunsearch \"*@beta\"\r\n```\r\n\r\n准备工作就到这里，准备开码', '1539263385', '1539395102', '0', '1', '118');
INSERT INTO `content_article` VALUES ('3', '2', '使用迅搜xunsearch给我的博客加上中文全文搜索（二）', '全文检索', '无', '', '<p>本文只阐述迅搜在我的博客中的应用，以作抛砖引玉。</p>\n<p>迅搜xunsearch做全文检索是用的索引服务器和搜索服务器两个服务来做的。</p>\n<ol><li>索引服务器，资料通过索引服务建立索引库以备查询使用。</li>\n<li>搜索服务器，搜索时通过本服务提供的接口返回数据。</li>\n</ol><p>迅搜中每个查询项目对应一个配置文件，/usr/local/xunsearch/data下会创建一个以项目名命名的文件夹，存放索引数据库。</p>\n<p>编写配制文件 <a href=\"http://www.xunsearch.com/doc/php/guide/ini.guide\" rel=\"nofollow noreferrer noopener\" target=\"_blank\">官方手册</a> 已经介绍的比较清晰了。</p>\n<pre><code>project.name = article\nproject.default_charset = UTF-8\n\n[id]\ntype = id\n\n[title]\ntype = title\n\n[content]\ntype = body\ncutlen = 300\ntokenizer = scws([3])\n\n[create_time]\ntokenizer = full\nweight = 0\n\n[state]\ntype = numeric\nindex = self\ntokenizer = full</code></pre>\n<p>搜索项目实例化</p>\n<pre><code>    $ini = env(\'app_path\').\'/common/lib/xunsearch/article.ini\',\n    $xs = new \\XS($ini);    // 项目实例</code></pre>\n<p>创建索引需要索引文档，此文档对象和搜索结果的文档对象并不是同一个。</p>\n<pre><code>    $docData = [\n        \'id\'=&gt;$data[\'id\'],\n        \'title\'=&gt;$data[\'title\'],\n        \'content\'=&gt;strip_tags($data[\'content\']),\n        \'create_time\'=&gt;$data[\'create_time\'],\n        \'state\'=&gt;1,\n    ];\n    // 实例化文档对象并赋值。\n    $doc = new \\XSDocument();\n    $doc-&gt;setFields($docData);</code></pre>\n<p>索引对象是迅搜项目对象上的一个属性</p>\n<pre><code>    $xs-&gt;index</code></pre>\n<p>向索引库中增加文档，需要注意的是相同的数据，可以多次添加，并且不报错……</p>\n<pre><code>    $xs-&gt;index-&gt;add($doc);</code></pre>\n<p>索引库中更新文档 需要注意的是 这里不能只更新变化的字段，而是所有字段都要向文档对象赋值，就像新增对象一样。</p>\n<pre><code>    $xs-&gt;index-&gt;update($doc);</code></pre>\n<p>索引中删除文档对象</p>\n<pre><code>    $xs-&gt;index-&gt;del(explode(\',\',$ids));</code></pre>\n<p>至此索引库中的增删改查都有了  就这么简单。</p>\n<p>然后是搜索功能</p>\n<p>说起来更加简单。</p>\n<pre><code>    $docs = $xs-&gt;search-&gt;setQuery($keyword.\" AND state:1\")\n                -&gt;setLimit($per_page,$offset)\n                -&gt;setFuzzy(true)\n                -&gt;search();\n    $count = $xs-&gt;search-&gt;getLastCount();</code></pre>\n<p>通过关键词只搜索启用的文章。\n自行实现分页代码</p>', '本文只阐述迅搜在我的博客中的应用，以作抛砖引玉。\r\n\r\n迅搜xunsearch做全文检索是用的索引服务器和搜索服务器两个服务来做的。\r\n\r\n1. 索引服务器，资料通过索引服务建立索引库以备查询使用。\r\n2. 搜索服务器，搜索时通过本服务提供的接口返回数据。\r\n\r\n迅搜中每个查询项目对应一个配置文件，/usr/local/xunsearch/data下会创建一个以项目名命名的文件夹，存放索引数据库。\r\n\r\n编写配制文件 [官方手册](http://www.xunsearch.com/doc/php/guide/ini.guide) 已经介绍的比较清晰了。\r\n\r\n```\r\nproject.name = article\r\nproject.default_charset = UTF-8\r\n\r\n[id]\r\ntype = id\r\n\r\n[title]\r\ntype = title\r\n\r\n[content]\r\ntype = body\r\ncutlen = 300\r\ntokenizer = scws([3])\r\n\r\n[create_time]\r\ntokenizer = full\r\nweight = 0\r\n\r\n[state]\r\ntype = numeric\r\nindex = self\r\ntokenizer = full\r\n```\r\n\r\n搜索项目实例化\r\n\r\n```\r\n    $ini = env(\'app_path\').\'/common/lib/xunsearch/article.ini\',\r\n    $xs = new \\XS($ini);	// 项目实例\r\n```\r\n\r\n创建索引需要索引文档，此文档对象和搜索结果的文档对象并不是同一个。\r\n\r\n```\r\n    $docData = [\r\n        \'id\'=>$data[\'id\'],\r\n        \'title\'=>$data[\'title\'],\r\n        \'content\'=>strip_tags($data[\'content\']),\r\n        \'create_time\'=>$data[\'create_time\'],\r\n        \'state\'=>1,\r\n    ];\r\n	// 实例化文档对象并赋值。\r\n    $doc = new \\XSDocument();\r\n    $doc->setFields($docData);\r\n```\r\n\r\n索引对象是迅搜项目对象上的一个属性\r\n```\r\n    $xs->index\r\n```\r\n\r\n向索引库中增加文档，需要注意的是相同的数据，可以多次添加，并且不报错……\r\n\r\n```\r\n    $xs->index->add($doc);\r\n```\r\n\r\n索引库中更新文档 需要注意的是 这里不能只更新变化的字段，而是所有字段都要向文档对象赋值，就像新增对象一样。\r\n\r\n```\r\n    $xs->index->update($doc);\r\n```\r\n\r\n索引中删除文档对象\r\n\r\n```\r\n    $xs->index->del(explode(\',\',$ids));\r\n```\r\n\r\n至此索引库中的增删改查都有了  就这么简单。\r\n\r\n然后是搜索功能\r\n\r\n说起来更加简单。\r\n```\r\n    $docs = $xs->search->setQuery($keyword.\" AND state:1\")\r\n                ->setLimit($per_page,$offset)\r\n                ->setFuzzy(true)\r\n                ->search();\r\n    $count = $xs->search->getLastCount();\r\n```\r\n通过关键词只搜索启用的文章。\r\n自行实现分页代码', '1539519452', '1539509452', '0', '1', '102');
INSERT INTO `content_article` VALUES ('4', '2', '使用elasticsearch实现中文检索 简述（一）', 'elasticsearch，全文检索', '无', '', '<p>本着学习来安装的elasticsearch，所以所有环境都是windows系统。</p>\n<h2>安装</h2>\n<h3>elasticsearch</h3>\n<p>官方下载zip，解压到本地，需要主意的是目录中不能出现空格（导致IK分词插件无法运行），谨慎起见汉字也不要使用了。</p>\n<p>解压完就行了，就这么绿色环保无污染。</p>\n<h3>安装IK分词器</h3>\n<p><a href=\"https://github.com/medcl/elasticsearch-analysis-ik/releases\" rel=\"nofollow noreferrer noopener\" target=\"_blank\">项目下载地址</a> 下载zip包接到到ik文件夹，放到elasticsearch/plugins下。</p>\n<p>其实到这里的安装就可以满足基本学习了，当然也许你需要深入使用，可能需要安装 <a href=\"https://www.elastic.co/downloads/kibana\" rel=\"nofollow noreferrer noopener\" target=\"_blank\">kibana</a></p>\n<h3>环境准备</h3>\n<p>因为elasticsearch是java世界的产物，所以还需要安装java环境，配置三个环境变量。\nJAVA_HOME  C:\\Program Files\\Java\\jdk1.8.0_181\nCLASS_PATH  C:\\Program Files\\Java\\jdk1.8.0_181\\bin\nPATH  C:\\Program Files\\Java\\jdk1.8.0_181\\bin</p>\n<h3>启动服务</h3>\n<p>激动人心的时候，好吧因为太简单了所以并不激动，双击elasticsearch/bin 目录下的 elasticsearch.bat 启动服务。这就妥了，去使用你的客户端操作es吧</p>', '本着学习来安装的elasticsearch，所以所有环境都是windows系统。\r\n\r\n## 安装\r\n\r\n### elasticsearch\r\n\r\n官方下载zip，解压到本地，需要主意的是目录中不能出现空格（导致IK分词插件无法运行），谨慎起见汉字也不要使用了。\r\n\r\n解压完就行了，就这么绿色环保无污染。\r\n\r\n### 安装IK分词器\r\n\r\n[项目下载地址](https://github.com/medcl/elasticsearch-analysis-ik/releases) 下载zip包接到到ik文件夹，放到elasticsearch/plugins下。\r\n\r\n其实到这里的安装就可以满足基本学习了，当然也许你需要深入使用，可能需要安装 [kibana](https://www.elastic.co/downloads/kibana)\r\n\r\n### 环境准备\r\n\r\n因为elasticsearch是java世界的产物，所以还需要安装java环境，配置三个环境变量。\r\nJAVA_HOME  C:\\Program Files\\Java\\jdk1.8.0_181\r\nCLASS_PATH  C:\\Program Files\\Java\\jdk1.8.0_181\\bin\r\nPATH  C:\\Program Files\\Java\\jdk1.8.0_181\\bin\r\n\r\n### 启动服务\r\n\r\n激动人心的时候，好吧因为太简单了所以并不激动，双击elasticsearch/bin 目录下的 elasticsearch.bat 启动服务。这就妥了，去使用你的客户端操作es吧\r\n\r\n', '1540910497', '1541832365', '0', '1', '93');
INSERT INTO `content_article` VALUES ('5', '2', '使用elasticsearch实现中文检索 简述（二）', 'elasticsearch，全文检索', '无', '', '<p>上回书说到elasticsearch软件的安装，现在已经可以通过client来连接服务器并进行操作了。</p>\n<h2>安装elasticsearch php 客户端</h2>\n<pre><code>composer require elasticsearch/elasticsearch</code></pre>\n<h2>创建client实例</h2>\n<p>基本使用</p>\n<pre><code>require \'vendor/autoload.php\';\n\nuse Elasticsearch\\ClientBuilder;\n\n$hosts = [\n      \'192.168.1.1:9200\'\n];\n\n$client = ClientBuilder::create()           // client构建器实例\n                    -&gt;setHosts($hosts)      // 设置主机\n                    -&gt;build();              // 构建实例</code></pre>\n<h2>索引管理</h2>\n<pre><code>$client-&gt;indices()  // 索引数据统计和显示索引信息\n\n$client-&gt;nodes()  // 节点数据统计和显示节点信息\n\n$client-&gt;cluster()  // 集群数据统计和显示集群信息\n\n$client-&gt;snapshot()  // 对集群和索引进行拍摄快照或恢复数据\n\n$client-&gt;cat()  // 执行Cat API命令（通常在命令行中使用）\n\n$client-&gt;indices(); // 索引管理器\n\n$client-&gt;index($params)  // 创建索引\n\n$client-&gt;bulk($params); // 批量创建索引\n\n$client-&gt;get($params);  // 获取文档\n\n$client-&gt;update($params); // 更新文档\n\n$client-&gt;delete($params); // 删除文档\n\n$client-&gt;search($params);  // 搜索文档</code></pre>', '上回书说到elasticsearch软件的安装，现在已经可以通过client来连接服务器并进行操作了。\r\n\r\n## 安装elasticsearch php 客户端\r\n\r\n```\r\ncomposer require elasticsearch/elasticsearch\r\n```\r\n\r\n## 创建client实例\r\n\r\n基本使用\r\n\r\n```\r\nrequire \'vendor/autoload.php\';\r\n\r\nuse Elasticsearch\\ClientBuilder;\r\n\r\n$hosts = [\r\n      \'192.168.1.1:9200\'\r\n];\r\n\r\n$client = ClientBuilder::create()           // client构建器实例\r\n                    ->setHosts($hosts)      // 设置主机\r\n                    ->build();              // 构建实例\r\n```\r\n\r\n## 索引管理\r\n\r\n```\r\n$client->indices()  // 索引数据统计和显示索引信息\r\n\r\n$client->nodes()  // 节点数据统计和显示节点信息\r\n\r\n$client->cluster()  // 集群数据统计和显示集群信息\r\n\r\n$client->snapshot()  // 对集群和索引进行拍摄快照或恢复数据\r\n\r\n$client->cat()  // 执行Cat API命令（通常在命令行中使用）\r\n\r\n$client->indices(); // 索引管理器\r\n\r\n$client->index($params)  // 创建索引\r\n\r\n$client->bulk($params); // 批量创建索引\r\n\r\n$client->get($params);  // 获取文档\r\n\r\n$client->update($params); // 更新文档\r\n\r\n$client->delete($params); // 删除文档\r\n\r\n$client->search($params);  // 搜索文档\r\n``` ', '1541165208', '1545117693', '0', '1', '238');
INSERT INTO `content_article` VALUES ('8', '2', '使用guzzle导入自定义cookies', '爬虫，guzzle', '无', '', '<p>写爬虫的时候，经常会需要登陆进行操作，原来通过爬虫模拟浏览器登陆的方式进行操作，费时费力，对于某些小项目，可以从浏览器登陆，然后把cookie导出给guzzle用，guzzle直接爬取。</p>\n<p>只截取了一段代码，我想应该能猜懂怎么用吧！</p>\n<pre><code>$co = explode(\';\',$co);\n$cookies = [];\nforeach ($co as $value){\n    list($name,$val) = explode(\'=\',$value);\n    $item = [\n        \'Name\'=&gt;$name,\n        \'Value\'=&gt;$val,\n        \'Domain\'=&gt;\'qq.com\'\n    ];\n\n    $cookies[] = $item;\n}\n$cookiejar = new \\GuzzleHttp\\Cookie\\CookieJar(false,$cookies);\n$c = new \\GuzzleHttp\\Client([\'cookies\'=&gt;$cookiejar]);</code></pre>', '写爬虫的时候，经常会需要登陆进行操作，原来通过爬虫模拟浏览器登陆的方式进行操作，费时费力，对于某些小项目，可以从浏览器登陆，然后把cookie导出给guzzle用，guzzle直接爬取。\r\n\r\n只截取了一段代码，我想应该能猜懂怎么用吧！\r\n\r\n```\r\n$co = explode(\';\',$co);\r\n$cookies = [];\r\nforeach ($co as $value){\r\n    list($name,$val) = explode(\'=\',$value);\r\n    $item = [\r\n        \'Name\'=>$name,\r\n        \'Value\'=>$val,\r\n        \'Domain\'=>\'qq.com\'\r\n    ];\r\n\r\n    $cookies[] = $item;\r\n}\r\n$cookiejar = new \\GuzzleHttp\\Cookie\\CookieJar(false,$cookies);\r\n$c = new \\GuzzleHttp\\Client([\'cookies\'=>$cookiejar]);\r\n```', '1545637947', '1545637989', '0', '1', '14');
INSERT INTO `content_article` VALUES ('9', '2', '从php语言理解多进程编程', '多进程', '无', '', '<p>以下内容全部在 linux 下执行，过程中会穿插少量的 windows 下的介绍，如果有时间建议阅读《深入理解计算机系统》《Unix环境高级编程》</p>\n<h1>多进程简介</h1>\n<p>本节内容转载自<a href=\"https://www.cnblogs.com/snailrun/p/5575533.html\" rel=\"nofollow noreferrer noopener\" target=\"_blank\">文章</a>\n对于 windows 来说，进程和线程的概念都是有着明确定义的，进程的概念对应于一个程序的运行实例(instance)，而线程则是程序代码执行的最小单元。也就是说 windows 对于进程和线程的定义是与经典OS课程中所教授的进程、线程概念相一致的。</p>\n<p>提供 API，CreateThread() 用于建立一个新的线程，传递线程函数的入口地址和调用参数给新建的线程，然后新线程就开始执行了。</p>\n<p>windows下，一个典型的线程拥有自己的堆栈、寄存器（包括程序计数器PC，用于指向下一条应该执行的指令在内存中的位置），而代码段、数据段、打开文件这些进程级资源是同一进程内多个线程所共享的。因此同一进程的不同线程可以很方便的通过全局变量（数据段）进行通信，大家都可以对数据段进行读写，这很方便，也被在安全性方面诟病，因为它要求程序员时刻意识到这些数据不是线程独立的。</p>\n<p>对于 linux 来说，则没有很明确的进程、线程概念。首先linux只有进程而没有线程，然而它的进程又可以表现得像 windows 下的线程。 linux 利用 fork() 和 exec 函数族来操作多线程。 fork() 函数可以在进程执行的任何阶段被调用，一旦调用，当前进程就被分叉成两个进程——父进程和子进程，两者拥有相同的代码段和暂时相同的数据段（虽然暂时相同，但从分叉开的时刻就是逻辑上的两个数据段了，之所以说是逻辑上的，是因为这里是“写时复制”机制，也就是，除非万不得已有一个进程对数据段进行了写操作，否则系统不去复制数据段，这样达到了负担最小），两者的区别在于fork()函数返回值，对于子进程来说返回为0，对于父进程来说返回的是子进程id，因此可以通过</p>\n<pre><code>if(fork()==0)\n    …\nelse\n    …</code></pre>\n<p>来让父子进程执行不同的代码段，从而实现“分叉”。</p>\n<p>exec 函数族的函数的作用则是启动另一个程序的新进程，然后完全用那个进程来代替自己（代码段被替换，数据段和堆栈被废弃，只保留原有进程id）。这样，如果在 fork() 之后，在子进程代码段里用 exec 启动另一个进程，就相当于 windows 下的  CreateThread() 的用处了，所以说 linux 下的进程可以表现得像 windows 下的线程。</p>\n<p>然而 linux 下的进程不能像 windows 下线程那样方便地通信，因为他们没有共享数据段、地址空间等。它们之间的通信是通过所谓 IPC(InterProcess Communication) 来进行的。具体有管道（无名管道用于父子进程间通信，命名管道可以用于任意两个进程间的通信）、共享内存（一个进程向系统申请一块可以被共享的内存，其它进程通过标识符取得这块内存，并将其连接到自己的地址空间中，效果上类似于 windows 下的多线程间的共享数据段），信号量，套接字。</p>\n<h1>php 多进程</h1>\n<p>PHP多进程已经十分成熟，可以用户</p>\n<h2>基础知识</h2>\n<p>根据前一节的介绍可以知道， windows 下是不能使用 fork 来开辟新进程的，php中提供了 <a href=\"http://php.net/manual/zh/function.proc-open.php\" rel=\"nofollow noreferrer noopener\" target=\"_blank\">proc_open</a> 等函数来进行进程控制，相关内容本文并不展开，如有精力单独写篇文章。</p>\n<p>下面单说 *nix 下的多进程，开始之前先提供两个基础学习的点<a href=\"http://php.net/manual/en/intro.posix.php\" rel=\"nofollow noreferrer noopener\" target=\"_blank\">posix（可移植操作系统接口）相关函数</a> <a href=\"http://php.net/manual/en/book.pcntl.php\" rel=\"nofollow noreferrer noopener\" target=\"_blank\">二</a></p>\n<h2>示例代码</h2>\n<p>为了了解 php 在 linux 下如何创建多进程，我们先展示一下一个父进程，创建两个子进程的代码；</p>\n<pre><code>for ($i=0;$i&lt;2;$i++) {\n    $pid = pcntl_fork(); // 程序在这里分了叉，子进程从这里开始，并且会将到此之前的所有变量等都继承。\n    if ($pid &gt; 0) {\n        // 分支进入父进程\n        line(\"当前创建了第{$i}个子进程,PID是{$pid}\");\n    } else {\n        // 分支进入子进程\n        line(\"我是一个子进程\");\n        exit(0);    // 子进程代码运行完毕后要执行退出\n    }\n}\nfunction line($msg){\n    echo $msg.PHP_EOL;\n}</code></pre>\n<h2>进程管理</h2>\n<p>当子进程创建之后将由父进程对子进程进行管理，父进程不对子进程进行回收会产生孤儿进程和僵尸进程。（建议阅读深入理解计算机系统进行补充）</p>\n<ul><li>孤儿进程的产生是因为：子进程的活还没干完，父进程就自己圆寂了，等到子进程干完活时发现自己成为了孤儿，自己所占用的资源没有得到释放。这个时候会被init进程收养，并适时回收。</li>\n<li>僵尸进程产生的原因是：子进程活干完了，父进程还在忙别的，年幼的子进程虽然没有成为孤儿，但是因为没有老子的管教成为了行尸走肉，就是传说中的僵尸进程，这个时候只要老爹还健在，这个子进程就不会被送福利院接收教育，init不能回收资源，导致子进程一直占用资源。这种情况对计算机资源产生了浪费。</li>\n</ul><p>父进程等待子进程返回的状态两个函数 [<code>pcntl_wait</code>]() <a href=\"http://php.net/manual/zh/function.pcntl-waitpid.php\" rel=\"nofollow noreferrer noopener\" target=\"_blank\"><code>pcntl_waitpid</code></a>,第二个参数可以决定是否挂起父进程。</p>\n<p>其他参见</p>\n<ul><li>pcntl_fork() - 在当前进程当前位置产生分支（子进程）。译注：fork是创建了一个子进程，父进程和子进程 都从fork的位置开始向下继续执行，不同的是父进程执行过程中，得到的fork返回值为子进程 号，而子进程得到的是0。</li>\n<li>pcntl_signal() - 安装一个信号处理器</li>\n<li>pcntl_wifexited() - 检查状态代码是否代表一个正常的退出。</li>\n<li>pcntl_wifstopped() - 检查子进程当前是否已经停止</li>\n<li>pcntl_wifsignaled() - 检查子进程状态码是否代表由于某个信号而中断</li>\n<li>pcntl_wexitstatus() - 返回一个中断的子进程的返回代码</li>\n<li>pcntl_wtermsig() - 返回导致子进程中断的信号</li>\n<li>pcntl_wstopsig() - 返回导致子进程停止的信号</li>\n<li>pcntl_waitpid() - 等待或返回fork的子进程状态</li>\n</ul><h1>进程间通信</h1>\n<p>常见的<a href=\"https://www.cnblogs.com/LUO77/p/5816326.html\" rel=\"nofollow noreferrer noopener\" target=\"_blank\">进程间通信</a>的方式有以下几种。</p>\n<ul><li>\n<p>管道pipe：管道是一种半双工的通信方式，数据只能单向流动，而且只能在具有亲缘关系的进程间使用。进程的亲缘关系通常是指父子进程关系。</p>\n</li>\n<li>\n<p>命名管道FIFO：有名管道也是半双工的通信方式，但是它允许无亲缘关系进程间的通信。</p>\n</li>\n<li>\n<p>消息队列MessageQueue：消息队列是由消息的链表，存放在内核中并由消息队列标识符标识。消息队列克服了信号传递信息少、管道只能承载无格式字节流以及缓冲区大小受限等缺点。</p>\n</li>\n<li>\n<p>共享存储SharedMemory：共享内存就是映射一段能被其他进程所访问的内存，这段共享内存由一个进程创建，但多个进程都可以访问。共享内存是最快的 IPC 方式，它是针对其他进程间通信方式运行效率低而专门设计的。它往往与其他通信机制，如信号两，配合使用，来实现进程间的同步和通信。</p>\n</li>\n<li>\n<p>信号量Semaphore：信号量是一个计数器，可以用来控制多个进程对共享资源的访问。它常作为一种锁机制，防止某进程正在访问共享资源时，其他进程也访问该资源。因此，主要作为进程间以及同一进程内不同线程之间的同步手段。</p>\n</li>\n<li>\n<p>套接字Socket：套解口也是一种进程间通信机制，与其他通信机制不同的是，它可用于不同及其间的进程通信。</p>\n</li>\n<li>\n<p>信号 ( sinal ) ： 信号是一种比较复杂的通信方式，用于通知接收进程某个事件已经发生。</p>\n</li>\n</ul><h2>先演示使用信号进行通信的代码</h2>\n<pre><code>declare(ticks=1);\n\npcntl_signal(SIGHUP,  \"sig_handler\",false);\n\n$child_pid = [];\nfor ($i=0;$i&lt;2;$i++) {\n    $pid = pcntl_fork(); // 程序在这里分了叉，子进程从这里开始，并且会将到此之前的所有变量等都继承。\n    if ($pid &gt; 0) {\n        // 分支进入父进程\n        line(\"当前创建了第{$i}个子进程,PID是{$pid}\");\n        $child_pid[$i] = $pid;\n    } else {\n        // 分支进入子进程\n        line(\"我是一个子进程{$i}\");\n        while (true){\n            pcntl_signal_dispatch();\n        }\n        exit($i+1);    // 子进程代码运行完毕后要执行退出\n    }\n}\n\nsleep(1);\n\nposix_kill($child_pid[0],SIGHUP);\n\nwhile ($rpid = pcntl_wait($status,WUNTRACED)){\n    if (-1 !== $rpid){\n        line($rpid.\'进程结束\');\n    }else{\n        line(\"没有子进程\");\n        exit(0);\n    }\n}\n\nfunction line($msg){\n    echo $msg.PHP_EOL;\n}\n\nfunction sig_handler($signo)\n{\n    line(\"收到信号\");\n    var_dump(SIGHUP,$signo);\n    exit(0);\n}</code></pre>\n<h2>再来演示管道通信</h2>\n<p>说道管道 用法和读写文件一样，按照官方说法，管道是一种特殊的文件。</p>\n<pre><code>declare(ticks=1);\n\n$pid = pcntl_fork(); // 程序在这里分了叉，子进程从这里开始，并且会将到此之前的所有变量等都继承。\n\n$pipe_file = \'./test.pipe\';\n// 创建命名管道\nif( !file_exists( $pipe_file ) ){\n    if( !posix_mkfifo( $pipe_file, 0666 ) ){\n        line(\'管道创建失败！\');\n        exit();\n    }\n}\n\nif ($pid &gt; 0) {\n    // 分支进入父进程\n    line(\"当前创建了子进程,PID是{$pid}\");\n    $fb = fopen( $pipe_file, \"r\" );\n    // 管道的读取和写入都会阻塞\n    $message = fread( $fb, 1024 );\n    line($message);\n} else {\n    // 分支进入子进程\n    sleep(1);\n    $fb = fopen( $pipe_file, \"w\" );\n    fwrite( $fb, \"我是一个子进程{$i}\");\n    exit(0);    // 子进程代码运行完毕后要执行退出\n}\n\npcntl_wait( $status );\n\nfunction line($msg){\n    echo $msg.PHP_EOL;\n}</code></pre>\n<h2>使用共享内存实现通信</h2>\n<p>php提供了两种实现共享内存的扩展</p>\n<ul><li>shmop 系类函数\n<ul><li>是基于字符串偏移量的方式进行查找和写入的</li>\n</ul></li>\n<li>Semaphore 扩展中的 sem 类函数\n<ul><li>是以key-value形式进行查找和写入的</li>\n</ul></li>\n</ul><p>以下是shmop示例代码</p>\n<pre><code>$pid = pcntl_fork(); // 程序在这里分了叉，子进程从这里开始，并且会将到此之前的所有变量等都继承。\n\n$pipe_file = \'./test.pipe\';\n// 创建共享内存\n$shm_key = ftok(__FILE__, \'t\');\n$shm_id = shmop_open($shm_key, \"c\", 0666, 1024);\n\nif ($pid &gt; 0) {\n    // 分支进入父进程\n    line(\"当前创建了子进程,PID是{$pid}\");\n    sleep(3);\n    $message = shmop_read($shm_id, 0, 100);\n    line($message);\n} else {\n    // 分支进入子进程\n    sleep(1);\n    $size = shmop_write($shm_id, \"我是一个子进程{$i}\".PHP_EOL, 0);\n    $size2 = shmop_write($shm_id, \'追加\'.PHP_EOL, $size+1);\n    var_dump($size,$size2);\n    shmop_delete($shm_id);\n    exit(0);    // 子进程代码运行完毕后要执行退出\n}\n\npcntl_wait( $status );\n\nfunction line($msg){\n    echo $msg.PHP_EOL;\n}\n\nshmop_close($shm_id);\n</code></pre>\n<p>sem 函数示例代码</p>\n<pre><code>$pid = pcntl_fork(); // 程序在这里分了叉，子进程从这里开始，并且会将到此之前的所有变量等都继承。\n\n$pipe_file = \'./test.pipe\';\n// 创建共享内存\n$key = ftok(__FILE__, \'a\');\n$share_key = 1;\n$shm_id = shm_attach($key, 1024, 0666);\n\nif ($pid &gt; 0) {\n    // 分支进入父进程\n    line(\"当前创建了子进程,PID是{$pid}\");\n    sleep(3);\n    $message = shm_get_var($shm_id, $share_key);\n    line($message);\n} else {\n    // 分支进入子进程\n    sleep(1);\n    $message1 = \"我是一个子进程{$i}\";\n    shm_put_var($shm_id, $share_key, $message1);\n    shm_remove($shm_id);\n    exit(0);    // 子进程代码运行完毕后要执行退出\n}\n\npcntl_wait( $status );\n\nfunction line($msg){\n    echo $msg.PHP_EOL;\n}\n\nshm_detach($shm_id);</code></pre>\n<h2>信号量</h2>\n<p>通常信号量是和共享内存联合使用的；信号量提供了一种所机制，防止多个进程对于内存资源的争抢。实现原子操作。</p>\n<pre><code>$key=ftok(__FILE__,\'t\');\n/**\n * 获取一个信号量资源\n * int $key [, int $max_acquire = 1 [, int $perm = 0666 [, int $auto_release = 1 ]]]\n * $max_acquire:最多可以多少个进程同时获取信号\n * $perm:权限 默认 0666\n * $auto_release：是否自动释放信号量\n */\n$sem_id=sem_get($key);\n// 获取信号\nsem_acquire($seg_id);\n\n/**\n * 原子性操作业务代码\n */\n\n// 释放信号量\nsem_release($seg_id);\n// 把次信号从系统中移除\nsem_remove($sem_id);</code></pre>\n<h1>deamon 守护进程</h1>\n<p>1.在后台运行。 </p>\n<p>为避免挂起控制终端将Daemon放入后台执行。方法是在进程中调用fork使父进程终止，让Daemon在子进程中后台执行。 </p>\n<pre><code>if($pid=pcntl_fork()) \n    exit(0); // 是父进程，结束父进程，子进程继续</code></pre>\n<p>2.脱离控制终端，登录会话和进程组</p>\n<p>有必要先介绍一下Linux中的进程与控制终端，登录会话和进程组之间的关系：进程属于一个进程组，进程组号（GID）就是进程组长的进程号（PID）。登录会话可以包含多个进程组。这些进程组共享一个控制终端。这个控制终端通常是创建进程的登录终端。 控制终端，登录会话和进程组通常是从父进程继承下来的。我们的目的就是要摆脱它们，使之不受它们的影响。方法是在第1点的基础上，调用setsid()使进程成为会话组长： posix_setsid();\n说明：当进程是会话组长时setsid()调用失败。但第一点已经保证进程不是会话组长。setsid()调用成功后，进程成为新的会话组长和新的进程组长，并与原来的登录会话和进程组脱离。由于会话过程对控制终端的独占性，进程同时与控制终端脱离。</p>\n<p>3.禁止进程重新打开控制终端</p>\n<p>现在，进程已经成为无终端的会话组长。但它可以重新申请打开一个控制终端。可以通过使进程不再成为会话组长来禁止进程重新打开控制终端：</p>\n<pre><code>if($pid=pcntl_fork())\nexit(0); // 结束第一子进程，第二子进程继续（第二子进程不再是会话组长）</code></pre>\n<p>4.关闭打开的文件描述符</p>\n<p>进程从创建它的父进程那里继承了打开的文件描述符。如不关闭，将会浪费系统资源，造成进程所在的文件系统无法卸下以及引起无法预料的错误。按如下方法关闭它们：</p>\n<p><code>fclose(STDIN),fclose(STDOUT),fclose(STDERR)关闭标准输入输出与错误显示。</code></p>\n<p>5.改变当前工作目录</p>\n<p>进程活动时，其工作目录所在的文件系统不能卸下。一般需要将工作目录改变到根目录。对于需要转储核心，写运行日志的进程将工作目录改变到特定目录如chdir(\"/\")</p>\n<p>6.重设文件创建掩模</p>\n<p>进程从创建它的父进程那里继承了文件创建掩模。它可能修改守护进程所创建的文件的存取位。为防止这一点，将文件创建掩模清除：umask(0);</p>\n<p>7.处理SIGCHLD信号</p>\n<p>处理SIGCHLD信号并不是必须的。但对于某些进程，特别是服务器进程往往在请求到来时生成子进程处理请求。如果父进程不等待子进程结束，子进程将成为僵尸进程（zombie）从而占用系统资源。如果父进程等待子进程结束，将增加父进程的负担，影响服务器进程的并发性能。在Linux下可以简单地将SIGCHLD信号的操作设为SIG_IGN。 signal(SIGCHLD,SIG_IGN);</p>\n<p>这样，内核在子进程结束时不会产生僵尸进程。这一点与BSD4不同，BSD4下必须显式等待子进程结束才能释放僵尸进程。关于信号的问题请参考</p>\n<pre><code>umask(0);\n$pid = pcntl_fork();\nif (-1 === $pid) {\n    exit(\'fork fail\');\n} elseif ($pid &gt; 0) {\n    exit(0);\n}\n// 第一次fork的子进程设置为回话组长，脱离终端。\nif (-1 === posix_setsid()) {\n    exit(\"setsid fail\");\n}\n// 第二次fork子进程 防止重新打开控制终端。\n$pid = pcntl_fork();\nif (-1 === $pid) {\n    exit(\"fork fail\");\n} elseif (0 !== $pid) {\n    exit(0);\n}\n\nchdir(\"/\");\n\necho \"111\";\nsleep(10);\necho \"222\";</code></pre>', '以下内容全部在 linux 下执行，过程中会穿插少量的 windows 下的介绍，如果有时间建议阅读《深入理解计算机系统》《Unix环境高级编程》\r\n\r\n# 多进程简介\r\n\r\n本节内容转载自[文章](https://www.cnblogs.com/snailrun/p/5575533.html)\r\n对于 windows 来说，进程和线程的概念都是有着明确定义的，进程的概念对应于一个程序的运行实例(instance)，而线程则是程序代码执行的最小单元。也就是说 windows 对于进程和线程的定义是与经典OS课程中所教授的进程、线程概念相一致的。\r\n\r\n提供 API，CreateThread() 用于建立一个新的线程，传递线程函数的入口地址和调用参数给新建的线程，然后新线程就开始执行了。\r\n\r\nwindows下，一个典型的线程拥有自己的堆栈、寄存器（包括程序计数器PC，用于指向下一条应该执行的指令在内存中的位置），而代码段、数据段、打开文件这些进程级资源是同一进程内多个线程所共享的。因此同一进程的不同线程可以很方便的通过全局变量（数据段）进行通信，大家都可以对数据段进行读写，这很方便，也被在安全性方面诟病，因为它要求程序员时刻意识到这些数据不是线程独立的。\r\n\r\n对于 linux 来说，则没有很明确的进程、线程概念。首先linux只有进程而没有线程，然而它的进程又可以表现得像 windows 下的线程。 linux 利用 fork() 和 exec 函数族来操作多线程。 fork() 函数可以在进程执行的任何阶段被调用，一旦调用，当前进程就被分叉成两个进程——父进程和子进程，两者拥有相同的代码段和暂时相同的数据段（虽然暂时相同，但从分叉开的时刻就是逻辑上的两个数据段了，之所以说是逻辑上的，是因为这里是“写时复制”机制，也就是，除非万不得已有一个进程对数据段进行了写操作，否则系统不去复制数据段，这样达到了负担最小），两者的区别在于fork()函数返回值，对于子进程来说返回为0，对于父进程来说返回的是子进程id，因此可以通过\r\n```\r\nif(fork()==0)\r\n    …\r\nelse\r\n    …\r\n```\r\n来让父子进程执行不同的代码段，从而实现“分叉”。\r\n\r\nexec 函数族的函数的作用则是启动另一个程序的新进程，然后完全用那个进程来代替自己（代码段被替换，数据段和堆栈被废弃，只保留原有进程id）。这样，如果在 fork() 之后，在子进程代码段里用 exec 启动另一个进程，就相当于 windows 下的  CreateThread() 的用处了，所以说 linux 下的进程可以表现得像 windows 下的线程。\r\n\r\n然而 linux 下的进程不能像 windows 下线程那样方便地通信，因为他们没有共享数据段、地址空间等。它们之间的通信是通过所谓 IPC(InterProcess Communication) 来进行的。具体有管道（无名管道用于父子进程间通信，命名管道可以用于任意两个进程间的通信）、共享内存（一个进程向系统申请一块可以被共享的内存，其它进程通过标识符取得这块内存，并将其连接到自己的地址空间中，效果上类似于 windows 下的多线程间的共享数据段），信号量，套接字。\r\n\r\n# php 多进程\r\n\r\nPHP多进程已经十分成熟，可以用户\r\n\r\n## 基础知识\r\n\r\n根据前一节的介绍可以知道， windows 下是不能使用 fork 来开辟新进程的，php中提供了 [proc_open](http://php.net/manual/zh/function.proc-open.php) 等函数来进行进程控制，相关内容本文并不展开，如有精力单独写篇文章。\r\n\r\n下面单说 *nix 下的多进程，开始之前先提供两个基础学习的点[posix（可移植操作系统接口）相关函数](http://php.net/manual/en/intro.posix.php) [二](http://php.net/manual/en/book.pcntl.php)\r\n\r\n## 示例代码\r\n\r\n为了了解 php 在 linux 下如何创建多进程，我们先展示一下一个父进程，创建两个子进程的代码；\r\n\r\n```\r\nfor ($i=0;$i<2;$i++) {\r\n    $pid = pcntl_fork(); // 程序在这里分了叉，子进程从这里开始，并且会将到此之前的所有变量等都继承。\r\n    if ($pid > 0) {\r\n        // 分支进入父进程\r\n        line(\"当前创建了第{$i}个子进程,PID是{$pid}\");\r\n    } else {\r\n        // 分支进入子进程\r\n        line(\"我是一个子进程\");\r\n        exit(0);    // 子进程代码运行完毕后要执行退出\r\n    }\r\n}\r\nfunction line($msg){\r\n    echo $msg.PHP_EOL;\r\n}\r\n```\r\n\r\n## 进程管理\r\n\r\n当子进程创建之后将由父进程对子进程进行管理，父进程不对子进程进行回收会产生孤儿进程和僵尸进程。（建议阅读深入理解计算机系统进行补充）\r\n\r\n- 孤儿进程的产生是因为：子进程的活还没干完，父进程就自己圆寂了，等到子进程干完活时发现自己成为了孤儿，自己所占用的资源没有得到释放。这个时候会被init进程收养，并适时回收。\r\n- 僵尸进程产生的原因是：子进程活干完了，父进程还在忙别的，年幼的子进程虽然没有成为孤儿，但是因为没有老子的管教成为了行尸走肉，就是传说中的僵尸进程，这个时候只要老爹还健在，这个子进程就不会被送福利院接收教育，init不能回收资源，导致子进程一直占用资源。这种情况对计算机资源产生了浪费。\r\n\r\n父进程等待子进程返回的状态两个函数 [`pcntl_wait`]() [`pcntl_waitpid`](http://php.net/manual/zh/function.pcntl-waitpid.php),第二个参数可以决定是否挂起父进程。\r\n\r\n其他参见\r\n\r\n- pcntl_fork() - 在当前进程当前位置产生分支（子进程）。译注：fork是创建了一个子进程，父进程和子进程 都从fork的位置开始向下继续执行，不同的是父进程执行过程中，得到的fork返回值为子进程 号，而子进程得到的是0。\r\n- pcntl_signal() - 安装一个信号处理器\r\n- pcntl_wifexited() - 检查状态代码是否代表一个正常的退出。\r\n- pcntl_wifstopped() - 检查子进程当前是否已经停止\r\n- pcntl_wifsignaled() - 检查子进程状态码是否代表由于某个信号而中断\r\n- pcntl_wexitstatus() - 返回一个中断的子进程的返回代码\r\n- pcntl_wtermsig() - 返回导致子进程中断的信号\r\n- pcntl_wstopsig() - 返回导致子进程停止的信号\r\n- pcntl_waitpid() - 等待或返回fork的子进程状态\r\n\r\n# 进程间通信\r\n\r\n常见的[进程间通信](https://www.cnblogs.com/LUO77/p/5816326.html)的方式有以下几种。\r\n\r\n- 管道pipe：管道是一种半双工的通信方式，数据只能单向流动，而且只能在具有亲缘关系的进程间使用。进程的亲缘关系通常是指父子进程关系。\r\n\r\n- 命名管道FIFO：有名管道也是半双工的通信方式，但是它允许无亲缘关系进程间的通信。\r\n\r\n- 消息队列MessageQueue：消息队列是由消息的链表，存放在内核中并由消息队列标识符标识。消息队列克服了信号传递信息少、管道只能承载无格式字节流以及缓冲区大小受限等缺点。\r\n\r\n- 共享存储SharedMemory：共享内存就是映射一段能被其他进程所访问的内存，这段共享内存由一个进程创建，但多个进程都可以访问。共享内存是最快的 IPC 方式，它是针对其他进程间通信方式运行效率低而专门设计的。它往往与其他通信机制，如信号两，配合使用，来实现进程间的同步和通信。\r\n\r\n- 信号量Semaphore：信号量是一个计数器，可以用来控制多个进程对共享资源的访问。它常作为一种锁机制，防止某进程正在访问共享资源时，其他进程也访问该资源。因此，主要作为进程间以及同一进程内不同线程之间的同步手段。\r\n\r\n- 套接字Socket：套解口也是一种进程间通信机制，与其他通信机制不同的是，它可用于不同及其间的进程通信。\r\n\r\n- 信号 ( sinal ) ： 信号是一种比较复杂的通信方式，用于通知接收进程某个事件已经发生。\r\n\r\n## 先演示使用信号进行通信的代码\r\n\r\n```\r\ndeclare(ticks=1);\r\n\r\npcntl_signal(SIGHUP,  \"sig_handler\",false);\r\n\r\n$child_pid = [];\r\nfor ($i=0;$i<2;$i++) {\r\n    $pid = pcntl_fork(); // 程序在这里分了叉，子进程从这里开始，并且会将到此之前的所有变量等都继承。\r\n    if ($pid > 0) {\r\n        // 分支进入父进程\r\n        line(\"当前创建了第{$i}个子进程,PID是{$pid}\");\r\n        $child_pid[$i] = $pid;\r\n    } else {\r\n        // 分支进入子进程\r\n        line(\"我是一个子进程{$i}\");\r\n        while (true){\r\n            pcntl_signal_dispatch();\r\n        }\r\n        exit($i+1);    // 子进程代码运行完毕后要执行退出\r\n    }\r\n}\r\n\r\nsleep(1);\r\n\r\nposix_kill($child_pid[0],SIGHUP);\r\n\r\nwhile ($rpid = pcntl_wait($status,WUNTRACED)){\r\n    if (-1 !== $rpid){\r\n        line($rpid.\'进程结束\');\r\n    }else{\r\n        line(\"没有子进程\");\r\n        exit(0);\r\n    }\r\n}\r\n\r\nfunction line($msg){\r\n    echo $msg.PHP_EOL;\r\n}\r\n\r\nfunction sig_handler($signo)\r\n{\r\n    line(\"收到信号\");\r\n    var_dump(SIGHUP,$signo);\r\n    exit(0);\r\n}\r\n```\r\n\r\n## 再来演示管道通信\r\n\r\n说道管道 用法和读写文件一样，按照官方说法，管道是一种特殊的文件。\r\n\r\n```\r\ndeclare(ticks=1);\r\n\r\n$pid = pcntl_fork(); // 程序在这里分了叉，子进程从这里开始，并且会将到此之前的所有变量等都继承。\r\n\r\n$pipe_file = \'./test.pipe\';\r\n// 创建命名管道\r\nif( !file_exists( $pipe_file ) ){\r\n    if( !posix_mkfifo( $pipe_file, 0666 ) ){\r\n        line(\'管道创建失败！\');\r\n        exit();\r\n    }\r\n}\r\n\r\nif ($pid > 0) {\r\n    // 分支进入父进程\r\n    line(\"当前创建了子进程,PID是{$pid}\");\r\n    $fb = fopen( $pipe_file, \"r\" );\r\n    // 管道的读取和写入都会阻塞\r\n    $message = fread( $fb, 1024 );\r\n    line($message);\r\n} else {\r\n    // 分支进入子进程\r\n    sleep(1);\r\n    $fb = fopen( $pipe_file, \"w\" );\r\n    fwrite( $fb, \"我是一个子进程{$i}\");\r\n    exit(0);    // 子进程代码运行完毕后要执行退出\r\n}\r\n\r\npcntl_wait( $status );\r\n\r\nfunction line($msg){\r\n    echo $msg.PHP_EOL;\r\n}\r\n```\r\n\r\n## 使用共享内存实现通信\r\n\r\nphp提供了两种实现共享内存的扩展\r\n\r\n- shmop 系类函数\r\n    - 是基于字符串偏移量的方式进行查找和写入的\r\n- Semaphore 扩展中的 sem 类函数\r\n    - 是以key-value形式进行查找和写入的\r\n \r\n以下是shmop示例代码\r\n```\r\n$pid = pcntl_fork(); // 程序在这里分了叉，子进程从这里开始，并且会将到此之前的所有变量等都继承。\r\n\r\n$pipe_file = \'./test.pipe\';\r\n// 创建共享内存\r\n$shm_key = ftok(__FILE__, \'t\');\r\n$shm_id = shmop_open($shm_key, \"c\", 0666, 1024);\r\n\r\nif ($pid > 0) {\r\n    // 分支进入父进程\r\n    line(\"当前创建了子进程,PID是{$pid}\");\r\n    sleep(3);\r\n    $message = shmop_read($shm_id, 0, 100);\r\n    line($message);\r\n} else {\r\n    // 分支进入子进程\r\n    sleep(1);\r\n    $size = shmop_write($shm_id, \"我是一个子进程{$i}\".PHP_EOL, 0);\r\n    $size2 = shmop_write($shm_id, \'追加\'.PHP_EOL, $size+1);\r\n    var_dump($size,$size2);\r\n    shmop_delete($shm_id);\r\n    exit(0);    // 子进程代码运行完毕后要执行退出\r\n}\r\n\r\npcntl_wait( $status );\r\n\r\nfunction line($msg){\r\n    echo $msg.PHP_EOL;\r\n}\r\n\r\nshmop_close($shm_id);\r\n\r\n```\r\n\r\nsem 函数示例代码\r\n\r\n```\r\n$pid = pcntl_fork(); // 程序在这里分了叉，子进程从这里开始，并且会将到此之前的所有变量等都继承。\r\n\r\n$pipe_file = \'./test.pipe\';\r\n// 创建共享内存\r\n$key = ftok(__FILE__, \'a\');\r\n$share_key = 1;\r\n$shm_id = shm_attach($key, 1024, 0666);\r\n\r\nif ($pid > 0) {\r\n    // 分支进入父进程\r\n    line(\"当前创建了子进程,PID是{$pid}\");\r\n    sleep(3);\r\n    $message = shm_get_var($shm_id, $share_key);\r\n    line($message);\r\n} else {\r\n    // 分支进入子进程\r\n    sleep(1);\r\n    $message1 = \"我是一个子进程{$i}\";\r\n    shm_put_var($shm_id, $share_key, $message1);\r\n    shm_remove($shm_id);\r\n    exit(0);    // 子进程代码运行完毕后要执行退出\r\n}\r\n\r\npcntl_wait( $status );\r\n\r\nfunction line($msg){\r\n    echo $msg.PHP_EOL;\r\n}\r\n\r\nshm_detach($shm_id);\r\n```\r\n\r\n## 信号量\r\n\r\n通常信号量是和共享内存联合使用的；信号量提供了一种所机制，防止多个进程对于内存资源的争抢。实现原子操作。\r\n\r\n```\r\n$key=ftok(__FILE__,\'t\');\r\n/**\r\n * 获取一个信号量资源\r\n * int $key [, int $max_acquire = 1 [, int $perm = 0666 [, int $auto_release = 1 ]]]\r\n * $max_acquire:最多可以多少个进程同时获取信号\r\n * $perm:权限 默认 0666\r\n * $auto_release：是否自动释放信号量\r\n */\r\n$sem_id=sem_get($key);\r\n// 获取信号\r\nsem_acquire($seg_id);\r\n\r\n/**\r\n * 原子性操作业务代码\r\n */\r\n\r\n// 释放信号量\r\nsem_release($seg_id);\r\n// 把次信号从系统中移除\r\nsem_remove($sem_id);\r\n```\r\n\r\n# deamon 守护进程\r\n\r\n1.在后台运行。 \r\n\r\n为避免挂起控制终端将Daemon放入后台执行。方法是在进程中调用fork使父进程终止，让Daemon在子进程中后台执行。 \r\n\r\n```\r\nif($pid=pcntl_fork()) \r\n    exit(0); // 是父进程，结束父进程，子进程继续\r\n```\r\n\r\n2.脱离控制终端，登录会话和进程组\r\n\r\n有必要先介绍一下Linux中的进程与控制终端，登录会话和进程组之间的关系：进程属于一个进程组，进程组号（GID）就是进程组长的进程号（PID）。登录会话可以包含多个进程组。这些进程组共享一个控制终端。这个控制终端通常是创建进程的登录终端。 控制终端，登录会话和进程组通常是从父进程继承下来的。我们的目的就是要摆脱它们，使之不受它们的影响。方法是在第1点的基础上，调用setsid()使进程成为会话组长： posix_setsid();\r\n说明：当进程是会话组长时setsid()调用失败。但第一点已经保证进程不是会话组长。setsid()调用成功后，进程成为新的会话组长和新的进程组长，并与原来的登录会话和进程组脱离。由于会话过程对控制终端的独占性，进程同时与控制终端脱离。\r\n\r\n3.禁止进程重新打开控制终端\r\n\r\n现在，进程已经成为无终端的会话组长。但它可以重新申请打开一个控制终端。可以通过使进程不再成为会话组长来禁止进程重新打开控制终端：\r\n\r\n```\r\nif($pid=pcntl_fork())\r\nexit(0); // 结束第一子进程，第二子进程继续（第二子进程不再是会话组长）\r\n```\r\n\r\n4.关闭打开的文件描述符\r\n\r\n进程从创建它的父进程那里继承了打开的文件描述符。如不关闭，将会浪费系统资源，造成进程所在的文件系统无法卸下以及引起无法预料的错误。按如下方法关闭它们：\r\n\r\n`fclose(STDIN),fclose(STDOUT),fclose(STDERR)关闭标准输入输出与错误显示。`\r\n\r\n5.改变当前工作目录\r\n\r\n进程活动时，其工作目录所在的文件系统不能卸下。一般需要将工作目录改变到根目录。对于需要转储核心，写运行日志的进程将工作目录改变到特定目录如chdir(\"/\")\r\n\r\n6.重设文件创建掩模\r\n\r\n进程从创建它的父进程那里继承了文件创建掩模。它可能修改守护进程所创建的文件的存取位。为防止这一点，将文件创建掩模清除：umask(0);\r\n\r\n7.处理SIGCHLD信号\r\n\r\n处理SIGCHLD信号并不是必须的。但对于某些进程，特别是服务器进程往往在请求到来时生成子进程处理请求。如果父进程不等待子进程结束，子进程将成为僵尸进程（zombie）从而占用系统资源。如果父进程等待子进程结束，将增加父进程的负担，影响服务器进程的并发性能。在Linux下可以简单地将SIGCHLD信号的操作设为SIG_IGN。 signal(SIGCHLD,SIG_IGN);\r\n\r\n这样，内核在子进程结束时不会产生僵尸进程。这一点与BSD4不同，BSD4下必须显式等待子进程结束才能释放僵尸进程。关于信号的问题请参考\r\n\r\n```\r\numask(0);\r\n$pid = pcntl_fork();\r\nif (-1 === $pid) {\r\n    exit(\'fork fail\');\r\n} elseif ($pid > 0) {\r\n    exit(0);\r\n}\r\n// 第一次fork的子进程设置为回话组长，脱离终端。\r\nif (-1 === posix_setsid()) {\r\n    exit(\"setsid fail\");\r\n}\r\n// 第二次fork子进程 防止重新打开控制终端。\r\n$pid = pcntl_fork();\r\nif (-1 === $pid) {\r\n    exit(\"fork fail\");\r\n} elseif (0 !== $pid) {\r\n    exit(0);\r\n}\r\n\r\nchdir(\"/\");\r\n\r\necho \"111\";\r\nsleep(10);\r\necho \"222\";\r\n```', '1545988734', '1546004044', '0', '1', '34');
INSERT INTO `content_article` VALUES ('10', '1', 'asd', 'asd,fgsd,asdasf', 'asdasdf', '/uploads/20190111/2a19c1b5555711c80991c1cbd04ee1cf.jpg', '<p><img src=\"/uploads/20190111/7245bb52c3c98a5d3fa815d5db7ea3f0.png\" alt=\"\" /></p>', '![](/uploads/20190111/7245bb52c3c98a5d3fa815d5db7ea3f0.png)', '1547189149', '1547189842', '1', '0', '0');

-- ----------------------------
-- Table structure for content_article_reply
-- ----------------------------
DROP TABLE IF EXISTS `content_article_reply`;
CREATE TABLE `content_article_reply` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `article_id` int(11) NOT NULL,
  `ip` int(11) NOT NULL,
  `name` varchar(25) NOT NULL,
  `content` varchar(255) NOT NULL,
  `qq` char(10) NOT NULL,
  `website` varchar(255) NOT NULL,
  `create_time` int(11) NOT NULL,
  `state` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of content_article_reply
-- ----------------------------
INSERT INTO `content_article_reply` VALUES ('3', '1', '0', '猫铃儿', '新的开始', '410136330', 'http://www.dazhetu.cn', '1541770858', '1');

-- ----------------------------
-- Table structure for content_attachment
-- ----------------------------
DROP TABLE IF EXISTS `content_attachment`;
CREATE TABLE `content_attachment` (
  `hash` char(32) NOT NULL,
  `path` varchar(255) NOT NULL,
  `create_time` int(11) NOT NULL,
  `size` int(11) unsigned NOT NULL,
  PRIMARY KEY (`hash`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of content_attachment
-- ----------------------------
INSERT INTO `content_attachment` VALUES ('06cde5906b787a58200dd8c13bbffb28', '/uploads/20180926/ab4669ac2af7cbc0ede45fd0a14a20e0.png', '1537931649', '32492');
INSERT INTO `content_attachment` VALUES ('2b6865bfa0d3509dfc38283bf31b8e68', '/uploads/20190111/3cd08e524b53a7d369f8a550dc03a113.png', '1547186370', '21474');
INSERT INTO `content_attachment` VALUES ('2ba2608a25a6b34de079b38957a09123', '/uploads/20181007/2018c8cbb31190505319c18fbeb6271a.jpg', '1538894409', '20684');
INSERT INTO `content_attachment` VALUES ('37bcfade3025ef31b13827b005c69704', '/uploads/20180814/b1c4adacbc7289868fa33d071e6a455b.png', '1534240210', '190184');
INSERT INTO `content_attachment` VALUES ('3c8961c92429c5a92cc44ae2b7dfce01', '/uploads/20180814/43a5f5c1010b4811aeebe2ca247bfac4.png', '1534240151', '1616');
INSERT INTO `content_attachment` VALUES ('64d37eca7d4ec3c70ba27103a77f32ff', '/uploads/20180814/693db0bd37e80801bd431449ad112ae6.jpg', '1534240160', '152586');
INSERT INTO `content_attachment` VALUES ('72bae0474d2106c1642b03aaaa492d82', '/uploads/20180926/24dac561d3c452e04980d5a4c7fe65b9.jpg', '1537930751', '2535');
INSERT INTO `content_attachment` VALUES ('ac096f3eaba5e22b7f717e0a72af0b26', '/uploads/20190111/7245bb52c3c98a5d3fa815d5db7ea3f0.png', '1547189087', '75932');
INSERT INTO `content_attachment` VALUES ('c643e16114112438178035e585e2dc78', '/uploads/20181010/16834b8dfcbc336085f41ed0636a80fc.jpg', '1539183289', '5443');
INSERT INTO `content_attachment` VALUES ('e44d2ddbcf9f1c20fbb0a18f3b6bdc40', '/uploads/20190111/2a19c1b5555711c80991c1cbd04ee1cf.jpg', '1547186096', '10230');
INSERT INTO `content_attachment` VALUES ('fe134da6f0c7fac92184a0db36ce834c', '/uploads/20180814/3a424041ca5c6d0c5128df1901eceefb.png', '1534239731', '31503');

-- ----------------------------
-- Table structure for content_category
-- ----------------------------
DROP TABLE IF EXISTS `content_category`;
CREATE TABLE `content_category` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL,
  `seo_keyword` varchar(255) NOT NULL,
  `seo_description` varchar(255) NOT NULL,
  `path` varchar(255) NOT NULL,
  `theme` varchar(255) NOT NULL COMMENT '模板',
  `is_deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `state` tinyint(1) unsigned NOT NULL COMMENT '显示隐藏',
  `content` text NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of content_category
-- ----------------------------
INSERT INTO `content_category` VALUES ('1', '0', 'linux', 'linux', 'linux系统学习', '0,1', 'index', '0', '1', '<p><br /></p>');
INSERT INTO `content_category` VALUES ('2', '0', '后端开发', '后端代码开发', '后端代码开发', '0,5', 'index', '0', '1', '<p><br /></p>');
INSERT INTO `content_category` VALUES ('3', '0', '前端开发', '前端开发', '前端开发', '0,3', 'index', '0', '1', '<p><br /></p>');
INSERT INTO `content_category` VALUES ('4', '0', 'MySQL', 'MySQL', 'MySQL', '0,4', 'index', '0', '1', '<p><br /></p>');
INSERT INTO `content_category` VALUES ('5', '0', '其他', '其他分类', '暂无合适分类的文章', '0,5', 'index', '0', '1', '<p><br /></p>');
INSERT INTO `content_category` VALUES ('6', '2', 'asd as d', 'asdf ', 'asdf ', '0,5,6', '', '1', '1', '<p>a<span style=\"font-weight:bold;\">sdfasdasd</span>f<span style=\"font-weight:bold;\"></span></p><p><br /></p>');
INSERT INTO `content_category` VALUES ('7', '6', 'asd', 'asd', 'asd', '0,5,6,7', '', '1', '0', '<p>asd</p>');

-- ----------------------------
-- Table structure for content_link
-- ----------------------------
DROP TABLE IF EXISTS `content_link`;
CREATE TABLE `content_link` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `href` varchar(255) NOT NULL,
  `state` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of content_link
-- ----------------------------
INSERT INTO `content_link` VALUES ('1', '我的旧博客', 'http://blog.dazhetu.cn', '1');
INSERT INTO `content_link` VALUES ('2', '王召波博客园', 'http://www.cnblogs.com/wangzhaobo', '1');
INSERT INTO `content_link` VALUES ('3', '韩志飞博客', 'http://www.hzfblog.com', '1');
INSERT INTO `content_link` VALUES ('4', '李涛的CSDN博客', 'https://blog.csdn.net/qq_32364281', '1');
INSERT INTO `content_link` VALUES ('5', '淘宝店铺', 'https://shop108270966.taobao.com/?spm=a230r.7195193.1997079397.51.402b6539m5krxq', '1');
INSERT INTO `content_link` VALUES ('6', '白云飞的博客', 'http://www.baiyf.com/', '1');

-- ----------------------------
-- Table structure for content_music
-- ----------------------------
DROP TABLE IF EXISTS `content_music`;
CREATE TABLE `content_music` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `author` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `pic` varchar(255) NOT NULL,
  `lrc` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of content_music
-- ----------------------------
INSERT INTO `content_music` VALUES ('3', '一百万个可能', '歌手：Christine Welch。所属专辑：一百万个可能。', 'http://music.163.com/song/media/outer/url?id=29722582.mp3', 'http://p1.music.126.net/SSGt30LAVJwW31-qreZDFA==/2532175280446455.jpg?param=130y130', '[00:18.19]幽静窗外满地片片寒花\n[00:24.66]一瞬间永恒的时差\n[00:29.61]窝在棉被里\n[00:35.59]倾听踏雪听沉默的声音\n[00:42.09]飘雪藏永恒的身影\n[00:47.40]雪树下等你\n[00:48.63]\n[00:53.70]在一瞬间有一百万个可能\n[00:56.10]该向前走或者继续等\n[01:00.46]这冬夜里有百万个不确定\n[01:04.87]渐入深夜或期盼天明\n[01:09.32]云空的泪一如冰凌结晶了\n[01:15.69]成雪花垂\n[01:18.01]这一瞬间有一百万个可能\n[01:21.57]窝进棉被或面对寒冷\n[01:28.22]\n[01:45.48]幽静寒风吹来一缕声音\n[01:51.97]一瞬间看着你走近\n[01:56.64]暖了我冬心\n[02:02.84]倾听踏雪听沉默的声音\n[02:09.32]飘雪藏永恒的身影\n[02:14.32]雪树下等你\n[02:16.29]\n[02:19.13]在一瞬间有一百万个可能\n[02:23.46]该向前走或者继续等\n[02:27.72]这冬夜里有百万个不确定\n[02:32.31]渐入深夜或期盼天明\n[02:36.15]云空的泪一如冰凌结晶了\n[02:43.02]成雪花垂\n[02:45.24]这一瞬间有一百万个可能\n[02:49.64]窝进棉被或面对寒冷\n[02:55.35]\n[03:13.69]那晚上会是哪个瞬间\n[03:15.04]说好的爱会不会改变\n[03:17.01]而你让我徘徊在千里之外\n[03:18.88]yeah你让我等了好久baby\n[03:21.56]突然间那是哪个瞬间\n[03:23.69]你终於出现就是那个瞬间\n[03:25.90]等了好久忍不住伸手那个瞬间\n[03:28.90]\n[03:29.76]在一瞬间有一百万个可能\n[03:33.32]该向前走或者继续等\n[03:37.48]这深夜里有百万个不确定\n[03:42.00]渐入冬林或走向街灯\n[03:46.15]云空的泪一如冰凌结晶了\n[03:53.74]成雪花垂\n[03:55.63]\n[03:56.03]这一瞬间有一百万个可能\n[03:59.57]暖这冬心或面对寒冷\n[04:03.89]该向前走或者继续等\n[04:08.18]渐入冬林或走向街灯\n[04:12.62]窝进棉被或面对寒冷\n[04:16.93]暖这冬心或面对寒冷\n[04:24.52]\n');
INSERT INTO `content_music` VALUES ('5', '晓之车【机动战士高达seed插曲】', '歌手：16err。所属专辑：晓之车【机动战士高达seed插曲】。', 'http://music.163.com/song/media/outer/url?id=437250974.mp3', 'http://p2.music.126.net/wLM-u5Wj-YMXuAipan35Eg==/18294773975029847.jpg?param=130y130', '[by:西木叶蒸虾]\n[00:10.26]風さそう木陰に俯せて泣いてる\n[00:19.57]見も知らぬ私を私が見ていた\n[00:29.19]逝く人の調べを奏でるギターラ\n[00:38.42]来ぬ人の嘆きに星は落ちて\n[00:47.90]行かないで、どんなに叫んでも\n[00:56.02]オレンジの花びら静かに揺れるだけ\n[01:06.77]やわらかな額に残された\n[01:14.92]手のひらの記憶遥か\n[01:20.70]とこしえのさよならつま弾く\n[01:34.65]\n[01:43.07]優しい手にすがる子供の心を\n[01:51.81]燃えさかる車輪は振り払い進む\n[02:00.58]逝く人の嘆きを奏でてギターラ\n[02:09.19]胸の糸激しく掻き鳴らして\n[02:17.54]哀しみに染まらない白さで\n[02:25.63]オレンジの花びら揺れてた夏の影に\n[02:35.49]やわらかな額を失くしても\n[02:43.07]赤く染めた砂遥か越えて行く\n[02:51.66]さよならのリズム\n[03:02.65]\n[03:06.18]想い出を焼き尽くして進む大地に\n[03:15.06]懐かしく芽吹いて行くものがあるの\n[03:23.70]\n[03:58.57]暁の車を見送って\n[04:05.98]オレンジの花びら揺れてる今も何処か\n[04:15.92]いつか見た安らかな夜明けを\n[04:23.39]もう一度手にするまで\n[04:28.92]消さないで灯火\n[04:36.55]車輪は廻るよ\n[04:47.14]\n');
INSERT INTO `content_music` VALUES ('6', '天空之城（经典钢琴版）（Cover：久石譲）', '歌手：dylanf。所属专辑：《天空之城》。', 'http://music.163.com/song/media/outer/url?id=864711417.mp3', 'http://p1.music.126.net/d4Km-iVxvkjKeaeV7U-51w==/109951163394461179.jpg?param=130y130', '');
INSERT INTO `content_music` VALUES ('7', '一直很安静', '歌手：群星。所属专辑：葫芦丝2006。', 'http://music.163.com/song/media/outer/url?id=5261239.mp3', 'http://p2.music.126.net/BX4Bshq_fFCzK5iF0qwijg==/108851651167060.jpg?param=130y130', '[offset:500]\n[00:00.00]一直很安静\n[00:04.50]阿桑\n[00:09.50]\n[00:14.50]\n[00:19.50]\n[00:26.24]空荡的街景\n[00:30.00]想找个人放感情\n[00:35.01]作这种决定\n[00:38.63]是寂寞与我为邻\n[00:43.63]我们的爱情\n[00:47.53]像你路过的风景\n[00:52.43]一直在进行\n[00:55.39]脚步却从来不会为我而停\n[02:57.66][01:57.04][01:02.11]给你的爱一直很安静\n[03:01.68][02:00.86][01:06.19]来交换你偶尔给的关心\n[03:06.27][02:05.47][01:10.82]明明是三个人的电影\n[03:10.38][02:09.58][01:14.86]我却始终不能有姓名\n[01:22.88]MUSIC\n[01:40.12]你说爱像云\n[01:43.76]要自在飘浮才美丽\n[01:49.20]我终于相信\n[01:52.19]分手的理由有时候很动听\n[02:14.21]给你的爱一直很安静\n[02:18.28]我从一开始就下定决心\n[02:22.83]以为自己要的是曾经\n[02:26.87]却发现爱一定要有回应\n[02:34.90]MUSIC\n[03:14.99]给你的爱一直很安静\n[03:19.03]除了泪在我的脸上任性\n[03:23.66]原来缘分是用来说明\n[03:27.71]你突然不爱我这件事情\n[03:35.51]\n');
INSERT INTO `content_music` VALUES ('8', '月光石', '歌手：ルルティア。所属专辑：最新热歌慢摇104。', 'http://music.163.com/song/media/outer/url?id=33223345.mp3', 'http://p2.music.126.net/9kAx7AtyLbLop_XhJy3y5w==/3415083117091737.jpg?param=130y130', '[by:SERENAToo]\n[ti:月光石]\n[ar:RURUTIA]\n[00:10.000]\n[00:15.000]\n[00:25.000]\n[00:29.280]ああ 摇らめく灯火がれきの街に\n[00:41.780]なりひびく透明な调べは ねがいを湛え\n[00:54.000]今やけ落ちた空は水の底へと沉んでいく\n[01:06.760]满ちていく月明かり\n[01:17.150]\n[01:18.370]青く燃える夜が君を染めるはかないほど\n[01:30.680]细い肩をきつく抱いていても\n[01:39.720]君は消えてしまいそうで\n[01:46.230]\n[01:57.610]ああ むすうの星粒宇宙の果てで\n[02:10.150]みつけたよ 初めての出会いは目眩ちえおぼえ\n[02:22.320]今降りそそぐ金と银の光の波に吞まれ\n[02:35.010]二人せいなる河へ\n[02:40.370]\n[02:46.470]甘く押し寄せてはぼくの胸をふるはせるよ\n[02:59.150]ずっとこのままずっと抱いていたい\n[03:08.190]君が消えてしまわないように\n[03:19.140]\n[03:36.320]今降りそそぐ金と银の光の波に吞まれ\n[03:42.530]二人せいなる河へ\n[03:58.450]青く燃える夜が君を染めるはかないほど\n[04:12.060]细い肩をきつく抱いていても\n[04:21.140]君は消えてしまいそうで\n[04:26.000]甘く押し寄せてはぼくの胸をふるはせるよ\n[04:36.010]ずっとこのままずっと抱いていたい\n[04:45.650]君が消えてしまわないように\n');
INSERT INTO `content_music` VALUES ('9', '全世界谁倾听你 （电影《从你的全世界路过》插曲）', '歌手：林宥嘉。所属专辑：全世界谁倾听你。', 'http://music.163.com/song/media/outer/url?id=432509483.mp3', 'http://p1.music.126.net/zGgUaGvXapKMy5tfQIB7cw==/109951163167743834.jpg?param=130y130', '[ti:]\n[ar:]\n[al:]\n[by:]\n[offset:0]\n[00:00.00] 作曲 : 梁翘柏\n[00:01.00] 作词 : 李焯雄\n[00:31.15]多希望有一个像你的人\n[00:36.15]但黄昏跟清晨无法相认\n[00:41.38]雨停了 歌停了 风继续\n[00:46.25]雨伞又遗落原地\n[00:51.63]多希望你就是最后的人\n[00:56.76]但年轮和青春不忍相认\n[01:01.87]一盏灯 一座城 找一人\n[01:06.72]一路的颠沛流离\n[01:14.09]从你的全世界路过\n[01:19.07]把全盛的我都活过\n[01:24.25]请往前走 不必回头\n[01:29.36]在终点等你的人会是我\n[01:43.18]多希望你就是最后的人\n[01:48.07]但年轮和青春不忍相认\n[01:53.14]一盏灯 一座城 找一人\n[01:58.03]一路的颠沛流离\n[02:05.37]从你的全世界路过\n[02:10.41]把全盛的爱都活过\n[02:15.82]我始终没说\n[02:18.75]不增加你负荷\n[02:21.38]最后等你的人是我\n[02:25.81]从你的全世界路过\n[02:30.98]把全盛的我都活过\n[02:36.70]请往前走 不必回头\n[02:41.37]在终点等你的人会是我\n[02:50.00]你爱默默倾听全世界\n[02:52.65]全世界谁倾听你\n[02:55.18]一朵一朵 一首一首的曾经\n[03:04.66]从你的全世界路过\n[03:09.87]把全盛的爱都活过\n[03:15.16]我始终没说\n[03:18.14]不增加你负荷\n[03:20.73]最后等你的人是我\n[03:25.18]从你的全世界路过\n[03:30.28]把全盛的我都活过\n[03:35.75]请往前走 不必回头\n[03:40.72]在终点等你的人会是我\n[03:48.89]请往前走 不必回头\n[03:54.86]在终点等你的人会是我\n');
INSERT INTO `content_music` VALUES ('10', '最美的期待', '歌手：周笔畅。所属专辑：最美的期待。', 'http://music.163.com/song/media/outer/url?id=531295576.mp3', 'http://p2.music.126.net/mwCUI0iL3xEC2a4WVICHlA==/109951163115369030.jpg?param=130y130', '[00:00.00] 作曲 : 南征北战\n[00:01.00] 作词 : 南征北战\n[00:20.38]我有一个梦像雨后彩虹\n[00:25.06]用所有泪水换来笑容\n[00:29.12]\n[00:29.78]还有一种爱穿越了人海\n[00:34.09]\n[00:34.68]拾起那颗迷失的尘埃\n[00:39.00]\n[00:39.53]你的呼吸越靠越近\n[00:43.67]\n[00:44.35]将我抱紧\n[00:46.72]\n[00:49.28]我睁开双眼想你在身边\n[00:53.89]无所谓永远还是瞬间\n[00:58.14]\n[00:58.69]静闭上了眼你却又浮现\n[01:03.52]带我远离寂寞的边缘\n[01:07.71]\n[01:08.26]忘了是非没有伤悲\n[01:13.16]无怨无悔\n[01:15.96]\n[01:17.95]我拥抱着爱当从梦中醒来\n[01:22.74]你执着地等待却不曾离开\n[01:27.58]舍不得分开在每一次醒来\n[01:32.42]不用再徘徊你就是我最美的期待\n[01:39.87]\n[02:01.15]我睁开双眼想你在身边\n[02:05.35]\n[02:05.87]无所谓永远还是瞬间\n[02:10.08]\n[02:10.61]静闭上了眼你却又浮现\n[02:15.57]带我远离寂寞的边缘\n[02:20.31]忘了是非没有伤悲\n[02:24.39]\n[02:25.00]无怨无悔\n[02:28.48]\n[02:29.92]我拥抱着爱当从梦中醒来\n[02:34.78]你执着地等待却不曾离开\n[02:39.56]舍不得分开在每一次醒来\n[02:44.39]不用再徘徊你就是我最美的期待\n[02:49.76]我拥抱着爱当从梦中醒来\n[02:53.92]你执着地等待却不曾离开\n[02:58.76]舍不得分开在每一次醒来\n[03:03.54]不用再徘徊你就是我最美的期待\n');
INSERT INTO `content_music` VALUES ('11', 'lucky one', '歌手：mich。所属专辑：lucky one。', 'http://music.163.com/song/media/outer/url?id=530986903.mp3', 'http://p1.music.126.net/fTk-hhwtMFM3iuxQqyK2oQ==/109951163116242395.jpg?param=130y130', '[by:yvngluv]\n[00:20.20]Your big brown eyes,\n[00:23.74]Stare straight back at mine\n[00:28.62]I have underlined,\n[00:34.55]The words i wanna say to you\n[00:39.18]Your rosy cheeks\n[00:43.36]And the way you smile\n[00:48.98]Is enough just to get me through\n[00:59.01]Flowers in your hair\n[01:01.45]Lipstick stains on my neck\n[01:03.69]The way you make me care\n[01:06.09]Without you i\'m a train wreck\n[01:08.88]Your lips on mine, i\'m the lucky one\n[01:17.80]Your perfect hands fit right into mine\n[01:26.91]I have underlined,\n[01:32.66]The things i wanna do for you\n[01:37.08]The way you sing,\n[01:41.13]Oh so sweet to me\n[01:46.81]Is enough to make me feel brand new\n[01:56.87]Flowers in your hair\n[01:59.31]Lipstick stains on my neck\n[02:01.61]The way you make me care\n[02:03.95]Without you i\'m a train wreck\n[02:06.49]Your lips on mine, i\'m the lucky one\n[02:16.20]Flowers in your hair\n[02:18.70]Lipstick stains on my neck\n[02:21.09]The way you make me care\n[02:23.38]Without you i\'m a train wreck\n[02:25.92]Your lips on mine, i\'m the lucky one\n');

-- ----------------------------
-- Table structure for content_tags
-- ----------------------------
DROP TABLE IF EXISTS `content_tags`;
CREATE TABLE `content_tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tag` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of content_tags
-- ----------------------------
INSERT INTO `content_tags` VALUES ('1', '博客');
INSERT INTO `content_tags` VALUES ('2', '全文检索');
INSERT INTO `content_tags` VALUES ('3', 'elasticsearch');
INSERT INTO `content_tags` VALUES ('4', '爬虫');
INSERT INTO `content_tags` VALUES ('5', 'guzzle');
INSERT INTO `content_tags` VALUES ('6', '多进程');
INSERT INTO `content_tags` VALUES ('7', 'asd');
INSERT INTO `content_tags` VALUES ('8', 'fgsd');
INSERT INTO `content_tags` VALUES ('9', 'asdasf');

-- ----------------------------
-- Table structure for content_tags_map
-- ----------------------------
DROP TABLE IF EXISTS `content_tags_map`;
CREATE TABLE `content_tags_map` (
  `tag_id` int(10) NOT NULL,
  `article_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of content_tags_map
-- ----------------------------
INSERT INTO `content_tags_map` VALUES ('1', '1');
INSERT INTO `content_tags_map` VALUES ('2', '2');
INSERT INTO `content_tags_map` VALUES ('2', '3');
INSERT INTO `content_tags_map` VALUES ('3', '4');
INSERT INTO `content_tags_map` VALUES ('2', '4');
INSERT INTO `content_tags_map` VALUES ('3', '5');
INSERT INTO `content_tags_map` VALUES ('2', '5');
INSERT INTO `content_tags_map` VALUES ('4', '8');
INSERT INTO `content_tags_map` VALUES ('5', '8');
INSERT INTO `content_tags_map` VALUES ('6', '9');
INSERT INTO `content_tags_map` VALUES ('7', '10');
INSERT INTO `content_tags_map` VALUES ('8', '10');
INSERT INTO `content_tags_map` VALUES ('9', '10');

-- ----------------------------
-- Table structure for site_config
-- ----------------------------
DROP TABLE IF EXISTS `site_config`;
CREATE TABLE `site_config` (
  `name` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  `label` varchar(255) NOT NULL,
  PRIMARY KEY (`name`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of site_config
-- ----------------------------
INSERT INTO `site_config` VALUES ('site_cip', '冀ICP备18021118号', '网站备案号');
INSERT INTO `site_config` VALUES ('site_name', '大者在途', '网站名称');

-- ----------------------------
-- Table structure for system_admin
-- ----------------------------
DROP TABLE IF EXISTS `system_admin`;
CREATE TABLE `system_admin` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(25) NOT NULL COMMENT '用户名',
  `password` varchar(255) NOT NULL COMMENT '密码',
  `name` varchar(10) NOT NULL COMMENT '姓名',
  `create_time` int(11) unsigned NOT NULL COMMENT '创建时间',
  `last_login` int(11) unsigned NOT NULL COMMENT '上次登录时间',
  `is_deleted` int(11) NOT NULL,
  `state` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of system_admin
-- ----------------------------
INSERT INTO `system_admin` VALUES ('1', 'admin', '$2y$11$CoZx4sie163txNYQOcmnPOuX8iZRyOR7EqQogh15r/nLZyPz6TDPW', 'tezsta', '0', '1546927463', '0', '1');
INSERT INTO `system_admin` VALUES ('2', 'bianji', '$2y$11$o/UilUmS6Fv.nqCBO5dFn.9d4RSoIMKhxMhZqJRt0gemXxEvDw5b2', 'tezsta', '1527521448', '1538294708', '0', '1');
INSERT INTO `system_admin` VALUES ('3', 'test', '$2y$11$vEqz0WCBtiR2N7yf1sWI8usb4qZ3WxkuyXpnG9ZVdGp3LI1ufTNr.', 'tezsta3', '0', '0', '0', '0');

-- ----------------------------
-- Table structure for system_auth_map
-- ----------------------------
DROP TABLE IF EXISTS `system_auth_map`;
CREATE TABLE `system_auth_map` (
  `admin_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of system_auth_map
-- ----------------------------
INSERT INTO `system_auth_map` VALUES ('1', '2');
INSERT INTO `system_auth_map` VALUES ('1', '3');
INSERT INTO `system_auth_map` VALUES ('2', '3');

-- ----------------------------
-- Table structure for system_auth_node
-- ----------------------------
DROP TABLE IF EXISTS `system_auth_node`;
CREATE TABLE `system_auth_node` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `path` varchar(255) NOT NULL,
  `auth` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0不限制 1登录 2授权',
  `level` int(1) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=281 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of system_auth_node
-- ----------------------------
INSERT INTO `system_auth_node` VALUES ('201', 'admin', 'admin', '0', '1');
INSERT INTO `system_auth_node` VALUES ('202', '后台管理员', 'admin/admin', '0', '2');
INSERT INTO `system_auth_node` VALUES ('203', '管理员列表', 'admin/admin/index', '2', '3');
INSERT INTO `system_auth_node` VALUES ('204', '添加操作', 'admin/admin/add', '2', '3');
INSERT INTO `system_auth_node` VALUES ('205', '编辑操作', 'admin/admin/edit', '2', '3');
INSERT INTO `system_auth_node` VALUES ('206', '删除及批量删除', 'admin/admin/del', '2', '3');
INSERT INTO `system_auth_node` VALUES ('207', '修改密码', 'admin/admin/password', '2', '3');
INSERT INTO `system_auth_node` VALUES ('208', '角色授权', 'admin/admin/role', '2', '3');
INSERT INTO `system_auth_node` VALUES ('209', '禁用/启用', 'admin/admin/change', '2', '3');
INSERT INTO `system_auth_node` VALUES ('210', '后台主页', 'admin/index', '0', '2');
INSERT INTO `system_auth_node` VALUES ('211', '后台框架首页', 'admin/index/index', '2', '3');
INSERT INTO `system_auth_node` VALUES ('212', '仪表盘', 'admin/index/dashboard', '2', '3');
INSERT INTO `system_auth_node` VALUES ('213', '登陆', 'admin/login', '0', '2');
INSERT INTO `system_auth_node` VALUES ('214', '登陆操作', 'admin/login/login', '2', '3');
INSERT INTO `system_auth_node` VALUES ('215', '退出操作', 'admin/login/logout', '2', '3');
INSERT INTO `system_auth_node` VALUES ('216', '菜单管理', 'admin/menu', '0', '2');
INSERT INTO `system_auth_node` VALUES ('217', '列表页', 'admin/menu/index', '2', '3');
INSERT INTO `system_auth_node` VALUES ('218', '添加', 'admin/menu/add', '2', '3');
INSERT INTO `system_auth_node` VALUES ('219', '修改', 'admin/menu/edit', '2', '3');
INSERT INTO `system_auth_node` VALUES ('220', '删除', 'admin/menu/del', '2', '3');
INSERT INTO `system_auth_node` VALUES ('221', '权限节点', 'admin/node', '0', '2');
INSERT INTO `system_auth_node` VALUES ('222', '列表', 'admin/node/index', '2', '3');
INSERT INTO `system_auth_node` VALUES ('223', '刷新节点', 'admin/node/clear', '2', '3');
INSERT INTO `system_auth_node` VALUES ('224', '角色管理', 'admin/role', '0', '2');
INSERT INTO `system_auth_node` VALUES ('225', '列表页', 'admin/role/index', '2', '3');
INSERT INTO `system_auth_node` VALUES ('226', '添加', 'admin/role/add', '2', '3');
INSERT INTO `system_auth_node` VALUES ('227', '编辑', 'admin/role/edit', '2', '3');
INSERT INTO `system_auth_node` VALUES ('228', '授权', 'admin/role/access', '2', '3');
INSERT INTO `system_auth_node` VALUES ('229', '广告管理', 'admin/content.advs', '0', '2');
INSERT INTO `system_auth_node` VALUES ('230', '列表页', 'admin/content.advs/index', '2', '3');
INSERT INTO `system_auth_node` VALUES ('231', '添加操作', 'admin/content.advs/add', '2', '3');
INSERT INTO `system_auth_node` VALUES ('232', '编辑操作', 'admin/content.advs/edit', '2', '3');
INSERT INTO `system_auth_node` VALUES ('233', '删除操作', 'admin/content.advs/del', '2', '3');
INSERT INTO `system_auth_node` VALUES ('234', '启用/禁用', 'admin/content.advs/change', '2', '3');
INSERT INTO `system_auth_node` VALUES ('235', '广告分类', 'admin/content.advs_category', '0', '2');
INSERT INTO `system_auth_node` VALUES ('236', '列表页', 'admin/content.advs_category/index', '2', '3');
INSERT INTO `system_auth_node` VALUES ('237', '添加操作', 'admin/content.advs_category/add', '2', '3');
INSERT INTO `system_auth_node` VALUES ('238', '编辑操作', 'admin/content.advs_category/edit', '2', '3');
INSERT INTO `system_auth_node` VALUES ('239', '禁用/启用', 'admin/content.advs_category/change', '2', '3');
INSERT INTO `system_auth_node` VALUES ('240', '删除操作', 'admin/content.advs_category/del', '2', '3');
INSERT INTO `system_auth_node` VALUES ('241', '文章管理', 'admin/content.article', '0', '2');
INSERT INTO `system_auth_node` VALUES ('242', '列表页', 'admin/content.article/index', '2', '3');
INSERT INTO `system_auth_node` VALUES ('243', '添加操作', 'admin/content.article/add', '2', '3');
INSERT INTO `system_auth_node` VALUES ('244', '编辑操作', 'admin/content.article/edit', '2', '3');
INSERT INTO `system_auth_node` VALUES ('245', '删除', 'admin/content.article/del', '2', '3');
INSERT INTO `system_auth_node` VALUES ('246', '禁用/启用', 'admin/content.article/change', '2', '3');
INSERT INTO `system_auth_node` VALUES ('247', '重建索引', 'admin/content.article/rebuild', '2', '3');
INSERT INTO `system_auth_node` VALUES ('248', '分类管理', 'admin/content.category', '0', '2');
INSERT INTO `system_auth_node` VALUES ('249', '列表页', 'admin/content.category/index', '2', '3');
INSERT INTO `system_auth_node` VALUES ('250', '添加操作', 'admin/content.category/add', '2', '3');
INSERT INTO `system_auth_node` VALUES ('251', '编辑操作', 'admin/content.category/edit', '2', '3');
INSERT INTO `system_auth_node` VALUES ('252', '删除操作', 'admin/content.category/del', '2', '3');
INSERT INTO `system_auth_node` VALUES ('253', '禁用/启用', 'admin/content.category/change', '2', '3');
INSERT INTO `system_auth_node` VALUES ('254', '友情链接', 'admin/content.link', '0', '2');
INSERT INTO `system_auth_node` VALUES ('255', '列表页', 'admin/content.link/index', '2', '3');
INSERT INTO `system_auth_node` VALUES ('256', '添加', 'admin/content.link/add', '2', '3');
INSERT INTO `system_auth_node` VALUES ('257', '编辑操作', 'admin/content.link/edit', '2', '3');
INSERT INTO `system_auth_node` VALUES ('258', '删除操作', 'admin/content.link/del', '2', '3');
INSERT INTO `system_auth_node` VALUES ('259', '禁用/启用', 'admin/content.link/change', '2', '3');
INSERT INTO `system_auth_node` VALUES ('260', '音乐管理', 'admin/content.music', '0', '2');
INSERT INTO `system_auth_node` VALUES ('261', '列表页', 'admin/content.music/index', '2', '3');
INSERT INTO `system_auth_node` VALUES ('262', '添加歌曲', 'admin/content.music/add', '2', '3');
INSERT INTO `system_auth_node` VALUES ('263', '删除歌曲', 'admin/content.music/del', '2', '3');
INSERT INTO `system_auth_node` VALUES ('264', '文章评论', 'admin/content.reply', '0', '2');
INSERT INTO `system_auth_node` VALUES ('265', '列表页', 'admin/content.reply/index', '2', '3');
INSERT INTO `system_auth_node` VALUES ('266', '禁用/启用', 'admin/content.reply/change', '2', '3');
INSERT INTO `system_auth_node` VALUES ('267', '删除操作', 'admin/content.reply/del', '2', '3');
INSERT INTO `system_auth_node` VALUES ('268', '标签管理', 'admin/content.tags', '0', '2');
INSERT INTO `system_auth_node` VALUES ('269', '列表页', 'admin/content.tags/index', '2', '3');
INSERT INTO `system_auth_node` VALUES ('270', '删除操作', 'admin/content.tags/del', '2', '3');
INSERT INTO `system_auth_node` VALUES ('271', '删除及批量删除', 'admin/role/del', '2', '3');
INSERT INTO `system_auth_node` VALUES ('272', '文件上传', 'admin/upload', '0', '2');
INSERT INTO `system_auth_node` VALUES ('273', '文件上传', 'admin/upload/file', '1', '3');
INSERT INTO `system_auth_node` VALUES ('274', '检查图片是否存在', 'admin/upload/checkfile', '1', '3');
INSERT INTO `system_auth_node` VALUES ('275', '百度umeditor富文本编辑器图片插入', 'admin/upload/ueditor', '1', '3');
INSERT INTO `system_auth_node` VALUES ('276', 'wangEditor富文本编辑器图片插入', 'admin/upload/wangeditor', '1', '3');
INSERT INTO `system_auth_node` VALUES ('277', 'markdown编辑器图片插入', 'admin/upload/markdown', '1', '3');
INSERT INTO `system_auth_node` VALUES ('278', '修改密码', 'admin/index/password', '2', '3');
INSERT INTO `system_auth_node` VALUES ('279', '修改个人信息', 'admin/index/editprofile', '2', '3');
INSERT INTO `system_auth_node` VALUES ('280', '清理缓存', 'admin/index/clearcache', '2', '3');

-- ----------------------------
-- Table structure for system_menu
-- ----------------------------
DROP TABLE IF EXISTS `system_menu`;
CREATE TABLE `system_menu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(20) NOT NULL,
  `url` varchar(255) NOT NULL,
  `icon` varchar(255) NOT NULL DEFAULT '',
  `pid` int(11) NOT NULL,
  `sort` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of system_menu
-- ----------------------------
INSERT INTO `system_menu` VALUES ('1', '系统核心', '#', '', '0', '50', '1');
INSERT INTO `system_menu` VALUES ('2', '系统设置', '#', '', '1', '50', '1');
INSERT INTO `system_menu` VALUES ('3', '权限节点', 'admin/node/index', '', '2', '50', '1');
INSERT INTO `system_menu` VALUES ('4', '菜单设置', 'admin/menu/index', '', '2', '50', '1');
INSERT INTO `system_menu` VALUES ('9', '角色授权', 'admin/role/index', '', '2', '50', '1');
INSERT INTO `system_menu` VALUES ('10', '后台用户', 'admin/admin/index', '', '2', '50', '1');
INSERT INTO `system_menu` VALUES ('11', '内容门户', '#', '', '0', '50', '0');
INSERT INTO `system_menu` VALUES ('12', '内容管理', '#', '', '11', '50', '0');
INSERT INTO `system_menu` VALUES ('13', '分类管理', 'admin/content.category/index', '', '12', '50', '0');
INSERT INTO `system_menu` VALUES ('14', '文章管理', 'admin/content.article/index', '', '12', '50', '0');
INSERT INTO `system_menu` VALUES ('15', '友情链接', 'admin/content.link/index', '', '11', '50', '0');
INSERT INTO `system_menu` VALUES ('16', '广告位管理', '#', '', '11', '50', '0');
INSERT INTO `system_menu` VALUES ('17', '广告分类', 'admin/content.advs_category/index', '', '16', '50', '0');
INSERT INTO `system_menu` VALUES ('18', '广告链接', 'admin/content.advs/index', '', '16', '50', '0');
INSERT INTO `system_menu` VALUES ('19', '网站配置', '#', '', '0', '49', '0');
INSERT INTO `system_menu` VALUES ('20', '基础配置', 'admin/config/edit', '', '19', '50', '0');
INSERT INTO `system_menu` VALUES ('21', '标签管理', 'admin/content.tags/index', '', '12', '50', '0');
INSERT INTO `system_menu` VALUES ('22', '音乐管理', 'admin/content.music/index', '', '11', '50', '0');
INSERT INTO `system_menu` VALUES ('23', '文章评论', 'admin/content.reply/index', '', '11', '50', '0');

-- ----------------------------
-- Table structure for system_role
-- ----------------------------
DROP TABLE IF EXISTS `system_role`;
CREATE TABLE `system_role` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `title` varchar(20) NOT NULL COMMENT '名称',
  `access_list` varchar(255) NOT NULL DEFAULT '' COMMENT '权限列表',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of system_role
-- ----------------------------
INSERT INTO `system_role` VALUES ('2', '管理员', '53,58,55,54,52,56,57,101,41,44,43,33,37,35,49,47,50,48,46');
INSERT INTO `system_role` VALUES ('3', '网站编辑', '90,93,92,91,89,96,99,98,97,95,72,75,74,73,71,67,77,76,68,66,80,83,82,81,79,105,104');
